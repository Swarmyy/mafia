﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mafia.Core
{
    public enum Alignment
    {
        Town,
        Mafia,
        Triad,
        Neutral
    }
}