﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mafia.Core
{
    public enum Categorie
    {
        Government,
        Investigative,
        Killing,
        Power,
        Protective,
        Random,
        Deception,
        Support,
        Benign
    }
}